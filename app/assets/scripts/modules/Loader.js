import $ from 'jquery';
class Loader {
    constructor() {
        $(window).on('load', function () {
            $(".loader").delay(50).fadeOut('slow');
        });
    }
}

export default Loader;

//const obj = new Loader();
//
//export default obj;